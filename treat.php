<?php
	try
		{
			$bdd = new PDO('mysql:host=localhost;dbname=joueurs', 'root', '');
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	$pseudo=$_POST['usrname'];
	$pass=($_POST['password']);

	$reponse = $bdd->prepare('SELECT id FROM informations WHERE pseudo = :pseudo AND password = :password');
	$reponse->execute(array(
		'pseudo' => $pseudo,
		'password' => $pass));
	$res = $reponse->fetch();
	if(!$res)
	{
		echo"Donnees incorrectes"; 
	}else
	{
		session_start();
		$_SESSION['pseudo'] = $pseudo;
		$_SESSION['id'] = $res['id'];
		header('Location: log_in.html');
	}
?>